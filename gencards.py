#!/usr/bin/env python3

import csv
import datetime
import random
import jinja2

numCards = int(input("Number of cards to make: "))
ValueNumber = int(input("Value of cards:  "))
BatchID = input("Batch ID:  ")
ExpDate = datetime.datetime.today() + datetime.timedelta(730)

cards = list()

rand = random.Random()
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

for i in range(0, numCards):
    cardNumber = "".join([rand.choice(numbers) for _ in range(16)])
    cardNumber = cardNumber[0:4]+"-"+cardNumber[4:8]+"-"+cardNumber[8:12]+"-"+cardNumber[12:16]
    cards.append([BatchID+"-"+cardNumber, BatchID, ValueNumber, ValueNumber, ExpDate.strftime("%Y-%m-%d"), ExpDate.strftime("%m/%d/%Y")])

with open(BatchID+".tnd", 'w', newline='') as cardFile:
    fieldnames=['CardNumber','BatchID','ValueNumber','Value','ExpDate','ExpDateDisplay']
    cardWriter = csv.writer(cardFile,
                            delimiter=',',
                            quotechar="'",
                            quoting=csv.QUOTE_MINIMAL)

    cardWriter.writerow(fieldnames)
    for row in cards:
        cardWriter.writerow(['"%s"' % item for item in row])

with open(BatchID+".html", 'w') as printFile:
    env = jinja2.Environment(loader=jinja2.loaders.FileSystemLoader("."))
    template = env.get_template("cards.html.j2")
    printFile.write(template.render(cardList=cards))
